C{
    #include <string.h> 
    #include <sodium.h>

    unsigned char key[crypto_shorthash_KEYBYTES]; 

    static void dp_hash(const struct vrt_ctx *ctx) {
        // define hash output buffer and key buffer
        unsigned char hash[crypto_shorthash_BYTES];
        char hexhash[2 * crypto_shorthash_BYTES + 1];
        char shorthash[3];

        // generate hash key
        crypto_shorthash_keygen(key);

        const struct gethdr_s hdr = { HDR_REQ, "\012page_id:" }; // assume max length of 10 (12 base 8) chars for page_id
        const char* page_id = VRT_GetHdr(ctx, &hdr);
        if (!page_id)
            return;

        // hash the page ID and put results into output buffer
        crypto_shorthash(hash, page_id, strlen(page_id), key);

        // convert hash to hex and shorten it to 3 chars
        sodium_bin2hex(hexhash, sizeof hexhash, hash, sizeof hash);
        memcopy(shorthash, hexhash, 3);

        // pass hash to the header to make sure it's accessible outside of this C{} block
        const struct gethdr_s hdr = {
            HDR_BERESP,
            "\003X-Hash:" // length prefixed string, in octal 
        };
        VRT_SetHdr(ctx, &hdr, shorthash, vrt_magic_string_end);
    }
}C

sub vcl_init {
    C{
        if (sodium_init() == -1) {
            return (1);
        }
        return (0);
    }C

    call reload_hash_key_vcl;
}

sub dp_hash_vcl {
    C{dp_hash(ctx);}C
}

sub reload_hash_key_vcl {
    C{
        crypto_shorthash_keygen(key);
    }C
}